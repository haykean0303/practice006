import React from "react";
import { Container, Table } from "react-bootstrap";

function TableData(props) {
  return (
    <Container>
      <h1>Table</h1>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>#</th>
            <th>Food</th>
            <th>Amount</th>
            <th>Price</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {props.items.map((item, i) => (
            <tr key={i}>
              {<td>{item.id}</td>}
              {<td>{item.title}</td>}
              {<td>{item.amount}</td>}
              {<td>{item.price}</td>}
              {<td>{item.total}</td>}
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
}

export default TableData;
